var AdminQuote = {
    data: null,
    xhr: null,
    categories: [],
    expanded: true,
    init: function (data) {
        AdminQuote.data = data;
        AdminQuote.loadElements();
        AdminQuote.bindElements();
    },
    loadElements: function () {},
    bindElements: function () {
        $(document).on('click', '#btn-back', AdminQuote.productPage);
        $(document).on('click', '#btn-update-product-information', AdminQuote.updateProductInformation);
        $(document).on('click', '.need-more-information a', AdminQuote.setUpfrontResponse);
        $(document).on('click', '.no-thank-you a', AdminQuote.setNoThankyouResponse);
        $(document).on('click', '#btn-clear-consignment', AdminQuote.clearConsignment);
        $(document).on('click', '#btn-upfront-purchase', AdminQuote.clearUpfrontPurchase);
        $(document).on('click', '#btn-upfront-purchase', AdminQuote.clearUpfrontPurchase);
        $(document).on('click', '#btn-save-quote', AdminQuote.saveQuote);
        $(document).on('click', '#btn-email-quote', AdminQuote.emailQuote);
        $(document).on('click', '#btn-need-more-info', AdminQuote.needMoreInfo);
        $(document).on('click', '#btn-no-thank-you', AdminQuote.noThankYou);
        $(document).on('keyup paste', '#couture_price', AdminQuote.computeConsignorPayout);
        $(document).on('change', '#commission', AdminQuote.computeConsignorPayout);
        $(document).on('submit', '#uploadForm', AdminQuote.updateItem);
    },
    updateItem: function (e) {
        e.preventDefault();
        var url = "/admin/quote/upload";
        $.ajax({
            type: "POST",
            url: url,
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (res) {
                if (res.result == true) {
                    toastr.success("Item successfully updated");
                    setTimeout(
                        function () {
                            location.reload();
                        }, 1000);
                } else {
                    toastr.error("Something went wrong");
                }
            },
            error: function (res) {}
        });
    },
    computeConsignorPayout: function () {
        var couture_price = $("#couture_price").val();
        var commission_percentage = ($("#commission").val() / 100) * couture_price;
        $('#consignor_payout').text(commission_percentage.toFixed(2));
    },
    saveQuote: function (e) {
        e.preventDefault();
        toastr.success("Quote has been saved");
    },
    emailQuote: function (e) {
        e.preventDefault();
        toastr.success("Quote has been emailed");
    },
    needMoreInfo: function (e) {
        e.preventDefault();
        toastr.success("Need more message sent");
    },
    noThankYou: function (e) {
        e.preventDefault();
        toastr.success("No thank you message sent");
    },
    clearConsignment: function () {
        $('#couture_price').val("");
        $('#couture_comment').val("");
        $('#consignor_payout').text("");
    },
    clearUpfrontPurchase: function () {
        $('#upfront-offer').val("");
        $('#upfront-purchase-comment').val("");
    },
    setUpfrontResponse: function (e) {
        e.preventDefault();
        var upFrontResponse = $(e.target).text();
        $("textarea#need-more-information-comment").val(upFrontResponse);
    },
    setNoThankyouResponse: function (e) {
        e.preventDefault();
        var noThankyouResponse = $(e.target).text();
        $("textarea#no-thank-you-comment").val(noThankyouResponse);
    },
    productPage: function () {
        window.location.href = "/admin/product";
    },
    backHistory: function (e) {
        e.preventDefault();
        window.history.back();
    },
    updateProductInformation: function () {
        AdminQuote.spinLoader();
        var url = '/api/update_quote_information';
        $.ajax({
            type: "POST",
            url: url,
            data: {
                quote_id: $("#btn-update-product-information").data('quote-id'),
                user_id: $("#btn-update-product-information").data('user-id'),
                quote_status: $('#quote_status').val(),
                first_name: $('#first_name').val(),
                last_name: $('#last_name').val(),
                email: $('#email').val(),
                phone: $('#phone').val(),
                item_category: $('#item_category').val(),
                designer: $('#designer').val(),
                description: $('#description').val(),
                condition: $('#condition').val(),
                size: $('#size').val(),
            },
            success: function (res) {
                if (res.result == true) {
                    AdminQuote.cancelSpinLoader();
                    toastr.success("Product Information Updated");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                } else {

                }
            },
            error: function (res) {},
        });
    },
    spinLoader: function () {
        $("#send-loader").addClass("spinner-border spinner-border-sm");
    },
    cancelSpinLoader: function () {
        $("#send-loader").removeClass("spinner-border spinner-border-sm")
    },
};
