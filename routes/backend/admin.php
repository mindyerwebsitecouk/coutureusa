<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\Auth\Product\ProductController;
use App\Http\Controllers\Backend\Auth\Quote\QuoteController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('product', [ProductController::class, 'index'])->name('product');
Route::get('quote/update/{id}', [QuoteController::class, 'update'])->name('update');
Route::post('quote/upload', [QuoteController::class, 'upload'])->name('upload');