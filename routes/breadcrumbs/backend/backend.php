<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});
Breadcrumbs::for('admin.product', function ($trail) {
    $trail->push('Product', route('admin.product'));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
