<li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-expanded="true"><i class="fas fa-list"></i> Main</a>
</li>
<li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#photos" role="tab" aria-controls="main" aria-expanded="true"><i class="fas fa-user"></i> Photos</a>
</li>
<li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#research" role="tab" aria-controls="main" aria-expanded="true"><i class="fas fa-comment"></i> Research</a>
</li>
<li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#quote" role="tab" aria-controls="main" aria-expanded="true"><i class="fas fa-clone"></i> Quote</a>
</li>

