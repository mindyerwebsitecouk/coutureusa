<!-- textbox content -->
<form id="uploadForm" enctype="multipart/form-data" action="/admin/quote/upload/9" method="post">
    @csrf
    <div class="row mt-4 mb-4">
        <div class="col">
            <div class="form-group row">
                <label class="col-md-2 form-control-label" for="quote_id">Quote ID</label>
                <input type="hidden" name="item_id_value" id="item_id_value" value="{{ $items->item_id }}" />
                <div class="col-md-10">
                    <label class="col-md-12 form-control-label" for="quote_id_value">{{ $items->quote_id }}</label>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 form-control-label" for="item_name">Item Name</label>
                <div class="col-md-10">
                    <label class="col-md-12 form-control-label" for="item_name">{{ $items->brand }} {{ $items->item_type }}</label>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 form-control-label" for="item_description">Description</label>
                <div class="col-md-10">
                    <label class="col-md-12 form-control-label" for="item_description">{{ $items->item_description }}</label>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2 form-control-label" for="exampleInputFile">Image</label>
                <div class="col-md-10">
                    <div style="padding: 2px;"><img src="{{ $itemImage }}" alt="Main Product Image" height="280" width="280"></div>
                    <input name="userImage" id="userImage" type="file" class="col-md-12 inputFile" />
                </div>
            </div>
            <!-- <div class="btn-group" style="padding: 3px;"> -->
            <!-- <a href="#" id="btn-add-files" class="btn btn-primary">Add Files</a> -->
            <!-- <a href="#" id="btn-start-upload" class="btn btn-success">Start Upload</a> -->
            <!-- </div> -->
        </div>
    </div>

    <div class="row" style="padding: 5px;">
        <div class="col text-right">
            <!-- <button class="btn btn-success btn-sm pull-right" data-user-id="{{ $user->id }}" data-quote-id="{{ $items->quote_id }}" id="btn-save-photo" type="submit"><span id="send-loader" class=""></span> Update</button> -->
            <input type="submit" value="Update Item" class="btn btn-success btn-sm pull-right">
        </div>
    </div>
</form>