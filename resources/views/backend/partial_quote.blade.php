<!-- textbox content -->
<div class="row mt-4 mb-4">
    <div class="col">
        <div class="row" style="margin-bottom: 25px;">
            <div class="col-sm-2 font-weight-bold">Quote Option</div>
            <div class="col-sm-5 font-weight-bold">Offer</div>
            <div class="col-sm-5 font-weight-bold">Comments</div>
        </div>
        <!-- consignment -->
        <div class="row" style="margin-bottom: 25px;">
            <div class="col-sm-2">Consignment</div>
            <div class="col-sm-5">
                <div class="form-group row">
                    <label class="col-md-3 form-control-label" for="">Couture Price (USD):</label>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="" id="couture_price" value="" placeholder="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 form-control-label" for="">Type</label>
                    <div class="col-md-9">
                        <label class="form-control-label" for="">{{ $items->item_type }}</label>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 form-control-label" for="">Designer</label>
                    <div class="col-md-9">
                        <select class="form-control" id="designer" disabled>
                            <?php if ($listBrands) : ?>
                                <?php foreach ($listBrands as $listBrands_) : ?>
                                    <option value="<?php echo $listBrands_->id; ?>" <?php echo ($listBrands_->id == $items->brand_id) ? 'selected' : ''; ?>><?php echo $listBrands_->name; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 form-control-label" for="">Commission %:</label>
                    <div class="col-md-9">
                        <select class="form-control" id="commission">
                            <option value="50">50 %</option>
                            <option value="70">70 %</option>
                            <option value="72.5">72.5 %</option>
                            <option value="75">75 %</option>
                            <option value="80">80 %</option>
                            <option value="85">85 %</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 form-control-label" for="">Consignor Payout:</label>
                    <div class="col-md-9">
                        <label>$<label id="consignor_payout" class="form-control-label" for=""></label> USD</label>
                    </div>
                </div>
                <div class="row" style="padding: 5px;">

                    <div class="col text-right">
                        <button class="btn btn-danger btn-sm pull-right" id="btn-clear-consignment" type="submit">Clear</button>
                    </div>
                </div>

            </div>
            <div class="col-sm-5">
                <textarea class="form-control rounded-0" id="couture_comment" rows="10"></textarea>
            </div>
        </div>
        <!-- end consignment -->
        <!-- Upfront Purchase -->
        <div class="row" style="margin-bottom: 25px;">
            <div class="col-sm-2">Upfront Purchase</div>
            <div class="col-sm-5">
                <div class="form-group row">
                    <label class="col-md-3 form-control-label" for="">Offer (USD):</label>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="" id="upfront-offer" value="" placeholder="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-12 form-control-label" for="">NOTE: Leave blank if no upfront purchase.</label>
                </div>
                <div class="row" style="padding: 5px;">

                    <div class="col text-right">
                        <button class="btn btn-danger btn-sm pull-right" id="btn-upfront-purchase" type="submit">Clear</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-5 upfront-purchase">
                <textarea class="form-control rounded-0" id="upfront-purchase-comment" rows="10"></textarea>
                <div class="btn-group float-right" style="padding: 3px;">
                    <a href="#" id="btn-save-quote" class="btn btn-primary">Save Quote</a>
                    <a href="#" id="btn-email-quote" class="btn btn-success">Email Quote</a>
                </div>
            </div>
        </div>
        <!-- end up front Purchase -->
        <!-- Need More Information -->
        <div class="row" style="margin-bottom: 25px;">
            <div class="col-sm-2">Need More Information</div>
            <div class="col-sm-5">
                <div class="form-group row">
                    <label class="col-md-12 form-control-label" for="">If additional information is required, please enter the information that is needed in the field to the right and click on the "More Info" button below.</label>
                </div>
            </div>
            <div class="col-sm-5">
                <textarea class="form-control rounded-0" id="need-more-information-comment" rows="10"></textarea>
                <div class="need-more-information"><a href="#">Please send photos.</a></div>
                <div class="need-more-information"><a href="#">Please provide item size.</a></div>
                <div class="need-more-information"><a href="#">Please describe item condition.</a></div>
                <div class="need-more-information"><a href="#">Please provide the item color.</a></div>

                <div class="btn-group float-right" style="padding: 3px;">
                    <a href="#" id="btn-need-more-info" class="btn btn-success">Need More Info</a>
                </div>
            </div>
        </div>
        <!-- end Need More Information -->
        <!-- No Thank You -->
        <div class="row" style="margin-bottom: 25px;">
            <div class="col-sm-2">Upfront Purchase</div>
            <div class="col-sm-5">
                <div class="form-group row">
                    <label class="col-md-12 form-control-label" for="">If additional comments are required, please enter the information that is needed in the field to the right and click on the "No Thank You" button below.</label>
                </div>
            </div>
            <div class="col-sm-5">
                <textarea class="form-control rounded-0" id="no-thank-you-comment" rows="10"></textarea>
                <div class="no-thank-you"><a href="#">Not accepting this brand/style/type/condition</a></div>
                <div class="no-thank-you"><a href="#">Item does not appear to be authentic.</a></div>

                <div class="btn-group float-right" style="padding: 3px;">
                    <a href="#" id="btn-no-thank-you" class="btn btn-success">No Thank You</a>
                </div>

            </div>
        </div>
        <!-- end No Thank You -->
    </div>

</div>
</div>

<div class="card">
    <div class="card-header">
        <strong>Quote Request Notes</strong>
    </div>
    <div class="card-body">
        <table id="quote-logs-quote-table" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Notes</th>
                    <th>User</th>
                    <th>Status</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Notes</th>
                    <th>User</th>
                    <th>Status</th>
                    <th>Date</th>
                </tr>
            </tfoot>
            <tbody>
                <?php if ($note) : ?>
                    <?php foreach ($note as $noteList) : ?>
                        <tr>
                            <td><span class="badge badge-env">Quote ID <?php echo $noteList->quote_id; ?> SUBMITTED. Quote status is set to <?php echo $noteList->status_name; ?></span></td>
                            <td>
                                <span class="badge level level-error">
                                    <i class="fa fa-fw fa-check"></i><?php echo $noteList->first_name; ?> <?php echo $noteList->last_name; ?>
                                </span>
                            </td>
                            <td><span class="badge badge-env"><?php echo $noteList->description; ?></span></td>
                            <td>
                                <span class="badge badge-default">
                                    <?php echo $noteList->created_at; ?>
                                </span>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>