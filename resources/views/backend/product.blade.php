@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title_product'))

@section('content')

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <strong>@lang('strings.backend.dashboard.welcome') {{ $logged_in_user->name }}!</strong>
            </div>
            <!--card-header-->
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            Quotes Product Management <small class="text-muted">Quotes List</small>
                        </h4>
                    </div>
                    <!--col-->
                </div>
                <hr>
                <table id="product-table" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>QuoteID</th>
                            <th>Item</th>
                            <th>Seller</th>
                            <th>Submit Date</th>
                            <th>Consign Quote</th>
                            <th>Purchase Quote</th>
                            <th>Status</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>QuoteID</th>
                            <th>Item</th>
                            <th>Seller</th>
                            <th>Submit Date</th>
                            <th>Consign Quote</th>
                            <th>Purchase Quote</th>
                            <th>Status</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php if ($items) : ?>
                            <?php foreach ($items as $item) : ?>
                                <tr>
                                    <td><a href="{!! url('/admin/quote/update/' . $item->quoteId); !!}" data-toggle="tooltip" title="Quote Id" class="limit-text" data-id="1"><?php echo $item->quoteId; ?></a></td>
                                    <td><a href="{!! url('/admin/quote/update/' . $item->quoteId); !!}" data-toggle="tooltip" title="Item" class="limit-text" data-id="1"><?php echo $item->brand; ?> <?php echo $item->item_type; ?></a></td>
                                    <td><a href="{!! url('/admin/quote/update/' . $item->quoteId); !!}" data-toggle="tooltip" title="Seller" class="limit-text" data-id="1"><?php echo $item->seller_last; ?>, <?php echo $item->seller_first; ?> </a></td>
                                    <td><a href="{!! url('/admin/quote/update/' . $item->quoteId); !!}" data-toggle="tooltip" title="Submit Date" class="limit-text warning" data-id="1"><?php echo $item->submitDate; ?></a></td>
                                    <td><a href="{!! url('/admin/quote/update/' . $item->quoteId); !!}" data-toggle="tooltip" title="Consign Quote" class="limit-text" data-id="1"><span class="badge badge-warning"><?php echo ($item->consignQuote) ? $item->consignQuote : 'Pending'; ?></span></a></td>
                                    <td><a href="{!! url('/admin/quote/update/' . $item->quoteId); !!}" data-toggle="tooltip" title="Purchase Quote" class="limit-text" data-id="1"><span class="badge badge-warning"><?php echo ($item->consignQuote) ? $item->consignQuote : 'Pending'; ?></span></a></td>
                                    <td><a href="{!! url('/admin/quote/update/' . $item->quoteId); !!}" data-toggle="tooltip" title="Status" class="limit-text" data-id="1"><span class="badge <?php echo ($item->status_id == 1) ? 'badge-warning' : 'badge-success'; ?>"><?php echo $item->status; ?></span></a></td>
                                    <!-- <td> -->
                                        <!-- <div class="btn-group btn-group-sm" role="group"> -->
                                        <!--      <button id="#" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</button>-->
                                        <!--  <div class="dropdown-menu" aria-labelledby="#">-->
                                        <!-- <a href="#" class="dropdown-item">Update Status</a> -->
                                        <!-- <a href="{!! url('/admin/quote/update/' . $item->quoteId); !!}" class="dropdown-item">Reply Quote</a> -->
                                        <!--     </div>-->
                                        <!-- </div>-->
                                    <!-- </td> -->
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <!--card-body-->
        </div>
        <!--card-->
    </div>
    <!--col-->
</div>
<!--row-->
@endsection
<!-- Javascript -->
{!! script('js/manifest.js') !!}
{!! script('js/vendor.js') !!}
{!! script('js/backend.js') !!}
{!! script('js/jquery-2.2.3.js') !!}
{!! script('js/dataTables.js') !!}
{!! script('js/dataTables.buttons-1.5.2.min.js') !!}
{!! script('js/jszip-3.1.3.min.js') !!}
{!! script('js/pdfmake-0.1.36.min.js') !!}
{!! script('js/vfs_fonts-0.1.36.js') !!}
{!! script('js/buttons.html5-1.5.2.min.js') !!}
{!! script('js/buttons.print-1.5.2.min.js') !!}
{!! script('js/buttons.colVis-1.5.2.min.js') !!}
{!! script('js/backendProduct.js') !!}
{!! script('js/bootstrap-3.3.6.min.js') !!}

<!-- css -->

{{ style('css/dataTables.min.css') }}
{{ style('css/backendProduct.css') }}

<script type="text/javascript">
    $(document).ready(function() {
        var data = {};
        AdminProduct.init(data);
        $('#product-table').DataTable({
            dom: 'lBfrtip',
            "scrollX": false,
            buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                'colvis',
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            columnDefs: [{
                bSortable: true
            }],
            "lengthMenu": [
                [50, 100, 150, 200, -1],
                [50, 100, 150, 200, "All"]
            ]
        });
    });
</script>