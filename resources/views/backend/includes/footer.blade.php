<footer class="app-footer">
    <div>
        <strong>Copyright Couture Usa &copy; {{ date('Y') }}
        </strong> @lang('strings.backend.general.all_rights_reserved')
    </div>

    <!-- <div class="ml-auto">Theme by <a href="https://coutureusa.com/">Couture Usa</a></div> -->
</footer>
