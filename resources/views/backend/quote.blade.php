@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title_product'))

@section('content')


<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <strong>@lang('strings.backend.dashboard.welcome') {{ $logged_in_user->name }}!</strong>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            Quotes Management <small class="text-muted">Now in Quote Request # {{ $items->quote_id }}</small>
                        </h4>
                    </div>
                    <div class="col-sm-7 text-right">
                        <button class="btn btn-primary btn-sm pull-right" id="btn-back" type="submit">Back</button>
                    </div>
                </div>
                <hr>
                <div class="row mt-4 mb-4">
                    <div class="col">
                        <ul class="nav nav-tabs" role="tablist">
                            @include('backend.partial_quote_top_link')
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="main" role="tabpanel" aria-expanded="true">
                                <div class="col">
                                    @include('backend.partial_quote_questions')
                                </div>
                                <div class="col">
                                    @include('backend.partial_quote_logs')
                                </div>
                            </div>
                            <div class="tab-pane" id="photos" role="tabpanel" aria-expanded="true">
                                <div class="col">
                                    @include('backend.partial_quote_photos')
                                </div>
                            </div>
                            <div class="tab-pane" id="research" role="tabpanel" aria-expanded="true">
                                <div class="col">
                                    @include('backend.partial_quote_photos_research')
                                </div>
                            </div>
                            <div class="tab-pane" id="quote" role="tabpanel" aria-expanded="true">
                                <div class="col">
                                @include('backend.partial_quote')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- end textbox content -->
</div>


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<!--row-->
@endsection
<!-- Javascript -->
{!! script('js/manifest.js') !!}
{!! script('js/vendor.js') !!}
{!! script('js/backend.js') !!}
{!! script('js/jquery-2.2.3.js') !!}
{!! script('js/dataTables.js') !!}
{!! script('js/dataTables.js') !!}
{!! script('js/dataTables.buttons-1.5.2.min.js') !!}
{!! script('js/jszip-3.1.3.min.js') !!}
{!! script('js/pdfmake-0.1.36.min.js') !!}
{!! script('js/vfs_fonts-0.1.36.js') !!}
{!! script('js/buttons.html5-1.5.2.min.js') !!}
{!! script('js/buttons.print-1.5.2.min.js') !!}
{!! script('js/buttons.colVis-1.5.2.min.js') !!}
{!! script('js/backendQuote.js') !!}
{!! script('js/bootstrap-3.3.6.min.js') !!}

<!-- css -->
{{ style('css/dataTables.min.css') }}
{{ style('css/backendProduct.css') }}

<script type="text/javascript">
    $(document).ready(function() {
        var data = {};
        AdminQuote.init(data);
        $('#quote-logs-table').DataTable({
            dom: 'lBfrtip',
            "scrollX": false,
            buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                'colvis',
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            columnDefs: [{
                bSortable: true
            }],
            "lengthMenu": [
                [50, 100, 150, 200, -1],
                [50, 100, 150, 200, "All"]
            ]
        });
        $('#quote-photo-search-table').DataTable({
            dom: 'lBfrtip',
            "scrollX": false,
            buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                'colvis',
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            columnDefs: [{
                bSortable: true
            }],
            "lengthMenu": [
                [50, 100, 150, 200, -1],
                [50, 100, 150, 200, "All"]
            ]
        });
        $('#quote-logs-quote-table').DataTable({
            dom: 'lBfrtip',
            "scrollX": false,
            buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                'colvis',
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            columnDefs: [{
                bSortable: true
            }],
            "lengthMenu": [
                [50, 100, 150, 200, -1],
                [50, 100, 150, 200, "All"]
            ]
        });
    });
</script>