<div class="card">
    <div class="card-header">
        <strong>Quote Request Notes</strong>
    </div>
    <div class="card-body">
        <table id="quote-logs-table" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Notes</th>
                    <th>User</th>
                    <th>Status</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Notes</th>
                    <th>User</th>
                    <th>Status</th>
                    <th>Date</th>
                </tr>
            </tfoot>
            <tbody>
                <?php if ($note) : ?>
                    <?php foreach ($note as $noteList) : ?>
                        <tr>
                            <td><span class="badge badge-env">Quote ID <?php echo $noteList->quote_id; ?> SUBMITTED. Quote status is set to <?php echo $noteList->status_name; ?></span></td>
                            <td>
                                <span class="badge level level-error">
                                    <i class="fa fa-fw fa-check"></i><?php echo $noteList->first_name; ?> <?php echo $noteList->last_name; ?>
                                </span>
                            </td>
                            <td><span class="badge badge-env"><?php echo $noteList->description; ?></span></td>
                            <td>
                                <span class="badge badge-default">
                                    <?php echo $noteList->created_at; ?>
                                </span>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <!--card-body-->
    </div>
    <!-- card footer -->
    <div class="row" style="padding: 5px;">
        <div class="col">
            <a class="btn btn-danger btn-sm" href="{!! url('/admin/product/'); !!}">Cancel</a>
        </div>
        <div class="col text-right">
            <button class="btn btn-success btn-sm pull-right" data-user-id="{{ $user->id }}" data-quote-id="{{ $items->quote_id }}" id="btn-update-product-information" type="submit"><span id="send-loader" class=""></span> Update Product Information</button>
        </div>
    </div>
</div>