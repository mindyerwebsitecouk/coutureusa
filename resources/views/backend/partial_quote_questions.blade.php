<!-- textbox content -->
<div class="row mt-4 mb-4">
    <div class="col">
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="quote_id">Quote ID</label>
            <div class="col-md-10">
                <label class="col-md-2 form-control-label" for="quote_id_value">{{ $items->quote_id }}</label>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="quote_status">Send Response</label>
            <div class="col-md-10">

                <select class="form-control" id="quote_status">
                    @foreach($quoteStatus as $quoteStatus_)
                    @if($quoteStatus_->id == $items->status_id)
                    <option value="{{ $quoteStatus_->id }}" selected>{{ $quoteStatus_->name }}</option>
                    @else
                    <option value="{{ $quoteStatus_->id }}">{{ $quoteStatus_->name }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="first_name">Seller First Name</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="first_name" id="first_name" value="{{ $items->first_name }}" placeholder="Seller First Name">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="last_name">Seller Last Name</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="last_name" id="last_name" value="{{ $items->last_name }}" placeholder="Last Name">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="email">E-mail Address</label>
            <div class="col-md-10">
                <input class="form-control" type="email" name="email" id="email" value="{{ $items->email }}" placeholder="E-mail Address">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="phone">Phone</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="phone" id="phone" value="{{ $items->phone1 }}" placeholder="phone">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="item_name">Item Name</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="item_name" id="item_name" value="{{ $items->brand }} {{ $items->item_type }}" placeholder="Item Name" disabled>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="item_category">Item Category</label>
            <div class="col-md-10">
                <select class="form-control" id="item_category">
                    <?php if ($listItemType) : ?>
                        <?php foreach ($listItemType as $listItemType_) : ?>
                            <option value="<?php echo $listItemType_->id; ?>" <?php echo ($listItemType_->id == $items->item_type_id) ? 'selected' : ''; ?>><?php echo $listItemType_->name; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <!-- <input class="form-control" type="text" name="item_category" id="item_category" value="{{ $items->item_type }}" placeholder="Item Category"> -->
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="email">Designer</label>
            <div class="col-md-10">
                <select class="form-control" id="designer">
                    <?php if ($listBrands) : ?>
                        <?php foreach ($listBrands as $listBrands_) : ?>
                            <option value="<?php echo $listBrands_->id; ?>" <?php echo ($listBrands_->id == $items->brand_id) ? 'selected' : ''; ?>><?php echo $listBrands_->name; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <!-- <input class="form-control" type="text" name="designer" id="designer" value="{{ $items->brand }}" placeholder="Designer"> -->
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="description">Description</label>
            <div class="col-md-10">
                <textarea class="form-control rounded-0" id="description" rows="10">{{ $items->item_description }}</textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="condition">Condition</label>
            <div class="col-md-10">
                <textarea class="form-control rounded-0" id="condition" rows="10">{{ $items->condition }}</textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="size">Size</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="size" id="size" value="{{ $items->size }}" placeholder="e.g. 2x2">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="accessories">Accessories</label>
            <div class="col-md-10">
                <label class="form-check-label" for="">Does your item include the following:</label>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="original_receipt">
                    <label class="form-check-label" for="original_receipt">Original Receipt</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="original_dust">
                    <label class="form-check-label" for="original_dust">Original Dust (if applicable)</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="original_box">
                    <label class="form-check-label" for="original_box">Original Box (if applicable)</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="original_authenticity">
                    <label class="form-check-label" for="original_authenticity">Original Authenticity (if applicable)</label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="questions">Questions</label>
            <div class="col-md-10">
                <label class="form-check-label" for="">Do you wish to consign or sell your item</label>
                <div class="form-check">
                    <label><input type="radio" name="consign_choices" checked> Consign my item with Couture Boutique</label>
                    <label><input type="radio" name="consign_choices"> Sell my item to Couture Boutique</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="item_owner">
                    <label class="form-check-label" for="original_dust">Are you the original owner of this item</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="original_box">
                    <label class="form-check-label" for="original_box">Original Box (if applicable)</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="original_authenticity">
                    <label class="form-check-label" for="original_authenticity">Original Authenticity (if applicable)</label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="purchase_location">Purchase Location</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="purchase_location" id="purchase_location" value="" placeholder="Purchase Location">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="purchase_date">Purchase Date</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="purchase_date" id="purchase_date" value="" placeholder="Purchase Date">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="purchase_price">Purchase Price</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="purchase_price" id="purchase_price" value="" placeholder="Purchase Price">
            </div>
        </div>
    </div>
</div>