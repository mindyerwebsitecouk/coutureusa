<!-- textbox content -->
<div class="row mt-4 mb-4">
    <div class="col">
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="quote_id">Quote ID</label>
            <div class="col-md-10">
                <label class="col-md-12 form-control-label" for="quote_id_value">{{ $items->quote_id }}</label>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="item_name">Item Name</label>
            <div class="col-md-10">
                <label class="col-md-12 form-control-label" for="item_name">{{ $items->brand }} {{ $items->item_type }}</label>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 form-control-label" for="item_description">Description</label>
            <div class="col-md-10">
                <label class="col-md-12 form-control-label" for="item_description">{{ $items->item_description }}</label>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <strong>Products previously sold by Couture</strong>
    </div>
    <div class="card-body">
        <table id="quote-photo-search-table" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Photo</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Entered On</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Photo</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Entered On</th>
                    <th>Status</th>
                </tr>
            </tfoot>
            <tbody>
                <!-- <td><span class="badge badge-env"><span> <img class="navbar-brand-full" src="{{ $itemImage }}" width="80" height="80" alt="Bag"></span></td>
            <td><span class="badge badge-env"><span> <span>Cartier Jewelry Apparel</span></td>
            <td><span class="badge badge-env"><span> <span>$100</span></td>
            <td><span class="badge badge-env"><span> <span>$200</span></td>
            <td><span class="badge badge-env"><span> <span>2019-06-25 18:17:45</span></td>
            <td><span class="badge badge-env"><span> <span>Sold</span></td> -->
                <?php if ($researchItemList) : ?>

                    <?php foreach ($researchItemList as $itemList) : ?>
                        <tr>
                            <td><span class="badge badge-env"><span> <img class="navbar-brand-full" src="{{ $itemImage }}" width="80" height="80" alt="Bag"></span></td>
                            <td><span class="badge badge-env"><span> <span>{{ $itemList->brands_name }} {{ $itemList->items_type_name }}</span></td>
                            <td><span class="badge badge-env"><span> <span>{{ $itemList->price }}</span></td>
                            <td><span class="badge badge-env"><span> <span>{{ $itemList->quotes_updated_at }}</span></td>
                            <td><span class="badge badge-env"><span> <span>{{ $itemList->item_status_name }}</span></td>
                        </tr>
                    <?php endforeach; ?>

                <?php endif; ?>
            </tbody>
        </table>

    </div>

</div>


<!-- <div class="row" style="padding: 5px;">
    <div class="col text-right">
        <button class="btn btn-success btn-sm pull-right" data-user-id="{{ $user->id }}" data-quote-id="{{ $items->quote_id }}" id="btn-save-photo" type="submit"><span id="send-loader" class=""></span> Send Response</button>
    </div>
</div> -->