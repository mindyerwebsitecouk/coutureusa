<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('request_id')->nullable();
            $table->integer('request_guid')->nullable();
            $table->integer('item_id')->nullable();
            $table->integer('seller_id')->nullable();
            $table->longText('note')->nullable();
            $table->integer('consign_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('remark_id')->nullable();
            $table->integer('reference_id')->nullable();
            $table->decimal('price', 8, 2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}
