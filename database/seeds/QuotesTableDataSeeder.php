<?php

use Illuminate\Database\Seeder;

class QuotesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quotes')->truncate();
        DB::table('quotes')->insert(
            [
                'request_id' => null,
                'request_guid' => null,
                'item_id' => 1,
                'seller_id' => 1,
                'note' => null,
                'consign_id' => null,
                'status_id' => 1,
                'remark_id' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('quotes')->insert(
            [
                'request_id' => null,
                'request_guid' => null,
                'item_id' => 2,
                'seller_id' => 2,
                'note' => null,
                'consign_id' => null,
                'status_id' => 1,
                'remark_id' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('quotes')->insert(
            [
                'request_id' => null,
                'request_guid' => null,
                'item_id' => 3,
                'seller_id' => 3,
                'note' => null,
                'consign_id' => null,
                'status_id' => 1,
                'remark_id' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('quotes')->insert(
            [
                'request_id' => null,
                'request_guid' => null,
                'item_id' => 4,
                'seller_id' => 4,
                'note' => null,
                'consign_id' => null,
                'status_id' => 1,
                'remark_id' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('quotes')->insert(
            [
                'request_id' => null,
                'request_guid' => null,
                'item_id' => 5,
                'seller_id' => 5,
                'note' => null,
                'consign_id' => null,
                'status_id' => 1,
                'remark_id' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
    }
}
