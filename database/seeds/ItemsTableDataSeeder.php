<?php

use Illuminate\Database\Seeder;

class ItemsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->truncate();
        DB::table('items')->insert(
            [
                'item_type_id' => 3,
                'brand_id' => 3,
                'name' => '',
                'description' => null,
                'size' => null,
                'condition' => null,
                'accessories_xml' => null,
                'purchase_xml' => null,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('items')->insert(
            [
                'item_type_id' => 3,
                'brand_id' => 10,
                'name' => '',
                'description' => null,
                'size' => null,
                'condition' => null,
                'accessories_xml' => null,
                'purchase_xml' => null,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('items')->insert(
            [
                'item_type_id' => 3,
                'brand_id' => 8,
                'name' => '',
                'description' => null,
                'size' => null,
                'condition' => null,
                'accessories_xml' => null,
                'purchase_xml' => null,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('items')->insert(
            [
                'item_type_id' => 3,
                'brand_id' => 10,
                'name' => '',
                'description' => null,
                'size' => null,
                'condition' => null,
                'accessories_xml' => null,
                'purchase_xml' => null,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('items')->insert(
            [
                'item_type_id' => 3,
                'brand_id' => 18,
                'name' => '',
                'description' => null,
                'size' => null,
                'condition' => null,
                'accessories_xml' => null,
                'purchase_xml' => null,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
    }
}
