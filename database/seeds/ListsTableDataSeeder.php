<?php

use Illuminate\Database\Seeder;

class ListsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lists')->truncate();
        DB::table('lists')->insert(
            [
                'quotes_id' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('lists')->insert(
            [
                'quotes_id' => 2,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('lists')->insert(
            [
                'quotes_id' => 3,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('lists')->insert(
            [
                'quotes_id' => 4,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('lists')->insert(
            [
                'quotes_id' => 5,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
    }
}
