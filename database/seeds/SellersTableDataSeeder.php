<?php

use Illuminate\Database\Seeder;

class SellersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sellers')->truncate();
        DB::table('sellers')->insert(
            [
                'first_name' => 'Amanda',
                'last_name' => 'Smith',
                'email' => 'amanda@yahoo.com',
                'phone1' => '',
                'phone2' => ''
            ]
        );
        DB::table('sellers')->insert(
            [
                'first_name' => 'Sarah',
                'last_name' => 'Bilhorn',
                'email' => 'sarah@yahoo.com',
                'phone1' => '',
                'phone2' => ''
            ]
        );
        DB::table('sellers')->insert(
            [
                'first_name' => 'Monica',
                'last_name' => 'Rodriguez',
                'email' => 'monica@yahoo.com',
                'phone1' => '',
                'phone2' => ''
            ]
        );
        DB::table('sellers')->insert(
            [
                'first_name' => 'Doreen',
                'last_name' => 'Hussman',
                'email' => 'doreen@yahoo.com',
                'phone1' => '',
                'phone2' => ''
            ]
        );
        DB::table('sellers')->insert(
            [
                'first_name' => 'Cynthia',
                'last_name' => 'Combs',
                'email' => 'cynthia@yahoo.com',
                'phone1' => '',
                'phone2' => ''
            ]
        );
       
    }
}
