<?php

use Illuminate\Database\Seeder;

class ItemsTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items_type')->truncate();
        DB::table('items_type')->insert(
            [
                'name' => 'Accessories',
            ]
        );
        DB::table('items_type')->insert(
            [
                'name' => 'Apparel',
            ]
        );
        DB::table('items_type')->insert(
            [
                'name' => 'Handbag',
            ]
        );
        DB::table('items_type')->insert(
            [
                'name' => 'Jewelry',
            ]
        );
        DB::table('items_type')->insert(
            [
                'name' => 'Luggage',
            ]
        );
        DB::table('items_type')->insert(
            [
                'name' => 'Mens',
            ]
        );
        DB::table('items_type')->insert(
            [
                'name' => 'Shoes',
            ]
        );
        DB::table('items_type')->insert(
            [
                'name' => 'Other',
            ]
        );
    }
}
