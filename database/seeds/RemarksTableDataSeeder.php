<?php

use Illuminate\Database\Seeder;

class RemarksTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('remarks')->truncate();
        DB::table('remarks')->insert(
            [
                'history' => 'Remarks goes here',
                'admin_id' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('remarks')->insert(
            [
                'history' => 'Remarks goes here',
                'admin_id' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('remarks')->insert(
            [
                'history' => 'Remarks goes here',
                'admin_id' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('remarks')->insert(
            [
                'history' => 'Remarks goes here',
                'admin_id' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
        DB::table('remarks')->insert(
            [
                'history' => 'Remarks goes here',
                'admin_id' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        );
      
    }
}
