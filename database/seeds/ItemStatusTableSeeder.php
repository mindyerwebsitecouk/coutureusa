<?php

use Illuminate\Database\Seeder;

class ItemStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_status')->truncate();
        DB::table('item_status')->insert(
            [
                'name' => 'Pending',
                'description' => null
            ]
        );
        DB::table('item_status')->insert(
            [
                'name' => 'Sold',
                'description' => null
            ]
        );
        DB::table('item_status')->insert(
            [
                'name' => 'Returned',
                'description' => null
            ]
        );
    }
}
