<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->truncate();
        DB::table('brands')->insert(
            [
                'name' => 'Balenciaga',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Bottega Veneta',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Burberry',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Breitling',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Bvlgari',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Cartier',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Celine',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Chanel',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Chopard',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Coach',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Christian Louboutin',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'David Yurman',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Fendi',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Goyard',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Gucci',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Hermes',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'John Hardy',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Louis Vuitton',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Manolo Blahnik',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Prada',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Rolex',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Tiffany & Co',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Tom Ford',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Valentino',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Van Cleef & Arpels',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Yves Saint Laurent',
            ]
        );
        DB::table('brands')->insert(
            [
                'name' => 'Other',
            ]
        );
    }
}
