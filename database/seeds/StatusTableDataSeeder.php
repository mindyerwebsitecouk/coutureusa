<?php

use Illuminate\Database\Seeder;

class StatusTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->truncate();
        DB::table('status')->insert(
            [
                'name' => 'Pending',
                'description' => 'Waiting for moderator to open'
            ]
        );
        DB::table('status')->insert(
            [
                'name' => 'Quote Sent',
                'description' => 'Moderator already replied to seller'
            ]
        );
        DB::table('status')->insert(
            [
                'name' => 'Need More Info',
                'description' => 'Waiting for more information'
            ]
        );
       
      
    }
}
