<?php

use Illuminate\Database\Seeder;

class ReferenceTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reference')->truncate();
        DB::table('reference')->insert(
            [
                'reference_number' => '00001',
            ]
        );
        DB::table('reference')->insert(
            [
                'reference_number' => '00002',
            ]
        );
        DB::table('reference')->insert(
            [
                'reference_number' => '00003',
            ]
        );
        DB::table('reference')->insert(
            [
                'reference_number' => '00004',
            ]
        );
        DB::table('reference')->insert(
            [
                'reference_number' => '00005',
            ]
        );
    }
}
