<?php

namespace App\Http\Controllers\Backend\Auth\Quote;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\QuoteRepository;
use App\Repositories\Backend\Auth\NoteRepository;
use App\Repositories\Backend\Auth\ItemRepository;

/**
 * Class ProductController.
 */
class QuoteController extends Controller
{

    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;
    protected $noteRepository;
    protected $itemRepository;
    protected $imageUploadPath = 'img/';
    protected $itemPrefix = 'itemId_';
    protected $itemNoImage = '/img/image-not-available.png';

    /**
     * PQuoteController constructor.
     *
     * @param ProductRepository $productRepository
     */
    public function __construct(
        QuoteRepository $quoteRepository,
        NoteRepository $noteRepository,
        ItemRepository $itemRepository
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->noteRepository = $noteRepository;
        $this->itemRepository = $itemRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function update(Request $request)
    {
        $user = auth()->user();
        $items = $this->quoteRepository->getItemByQuote($request->id);
        $quoteStatus = $this->quoteRepository->getQuoteStatus();
        $note = $this->noteRepository->getNote($request->id);
        $listItemType = $this->quoteRepository->getItemType();
        $listBrands = $this->quoteRepository->getBrands();
        $itemPath = $this->itemRepository->getItemImage($items->item_id);
        $itemImage = isset($itemPath->image_name) ? $itemPath->image_name : $this->itemNoImage;
        $researchItemList = $this->itemRepository->getItems();

        return view('backend.quote', compact('items', 'quoteStatus', 'user', 'note', 'listItemType', 'listBrands', 'itemImage', 'researchItemList'));
    }

    /**
     * Upload / update item
     */
    public function upload(Request $request)
    {
        if ($request) {
            $itemId = $request->item_id_value;
            if ($_FILES['userImage']['name']) {
                $uploadSavePath = $this->upload_product_image($itemId, $_FILES);
                $result = $this->itemRepository->insertItemImage($itemId, $uploadSavePath);
                $response = array('result' => $result);
            }           
        } else {
            $response = array('result' => false);
        }
        return response()->json($response);
    }

    /**
     * Create folder and upload image
     */
    public function upload_product_image($itemId, $uploaded)
    {
        if (is_uploaded_file($uploaded['userImage']['tmp_name'])) {
            $sourcePath = $uploaded['userImage']['tmp_name'];
            $dir = $this->imageUploadPath . $this->itemPrefix . $itemId . '/';
            $permit = 0777;
            if (file_exists($dir)) { } else {
                mkdir($dir);
            }
            chmod($dir, $permit);
            $uploadedFileName = $uploaded['userImage']['name'];
            $targetPath = $dir . $uploadedFileName;
            if (file_exists($targetPath)) {
                unlink($targetPath);
            }
            if (move_uploaded_file($sourcePath, $targetPath)) {
                return '/' . $dir . $uploadedFileName;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
