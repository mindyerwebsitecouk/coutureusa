<?php

namespace App\Http\Controllers\Backend\Auth\Product;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\ProductRepository;

/**
 * Class ProductController.
 */
class ProductController extends Controller
{

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * ProductController constructor.
     *
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        $this->productRepository = $productRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $items = $this->productRepository->getItems();
        return view('backend.product')->with('items', $items);
    }
}
