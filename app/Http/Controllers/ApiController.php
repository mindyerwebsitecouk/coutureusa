<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\ProductRepository;
use App\Repositories\Backend\Auth\QuoteRepository;
use App\Repositories\Backend\Auth\NoteRepository;

/**
 * Class ProductController.
 */
class ApiController extends Controller
{


    protected $productRepository;
    protected $quoteRepository;
    protected $noteRepository;

    /**
     * ProductController constructor.
     *
     * @param ProductRepository $productRepository
     */
    public function __construct(
        ProductRepository $productRepository,
        QuoteRepository $quoteRepository,
        NoteRepository $noteRepository
    ) {

        $this->productRepository = $productRepository;
        $this->quoteRepository = $quoteRepository;
        $this->noteRepository = $noteRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function propose_item(Request $request)
    {
        $bodyContent = json_decode($request->getContent());
        $items = $this->productRepository->proposeItem($bodyContent->payload);

        if ($items)
            $response = array('result' => true, 'data' => $items);
        else
            $response = array('result' => false, 'data' => null);

        return response()->json($response);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function update_quote_information(Request $request)
    {
        if ($request->quote_id) {
            $result = $this->quoteRepository->updateQuoteItems($request);
            $note = $this->noteRepository->insertNote($request);
            $response = array('result' => true);
        } else {
            $response = array('result' => false);
        }

        return response()->json($response);
    }
}
