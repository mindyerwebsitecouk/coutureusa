<?php

namespace App\Models\Auth;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Admins extends Model {

    public $timestamps = false;
    protected $table = 'admins';

}
