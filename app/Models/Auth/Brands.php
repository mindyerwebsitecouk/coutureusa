<?php

namespace App\Models\Auth;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Brands extends Model {

    public $timestamps = false;
    protected $table = 'brands';

}
