<?php

namespace App\Models\Auth;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Quotes extends Model {

    public $timestamps = false;
    protected $table = 'quotes';

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

}
