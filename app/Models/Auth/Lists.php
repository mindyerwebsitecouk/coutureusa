<?php

namespace App\Models\Auth;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Lists extends Model
{

    public $timestamps = false;
    protected $table = 'lists';

    /**
	 	eloquent one-to-many relationship between lists and quotes
     **/
    public function quotes()
    {
        return $this->belongsTo(Quotes::class, 'quotes_id');
    }
}
