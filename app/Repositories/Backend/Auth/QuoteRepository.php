<?php

namespace App\Repositories\Backend\Auth;

use DB;
use Carbon\Carbon;
use App\Models\Auth\Products;
use App\Models\Auth\Lists;
use App\Models\Auth\Quotes;
use App\Models\Auth\Items;
use App\Models\Auth\Items_type;
use App\Models\Auth\Brands;
use App\Models\Auth\Sellers;
use App\Models\Auth\Status;

/**
 * Class QuoteRepository.
 */
class QuoteRepository
{

    protected $defaultPendingStatus = 1;

    /**
     * Get Active Item list by quotation id
     * @param int
     * @return Object
     */
    public function getItemByQuote($id)
    {
        $lists = new Lists();
        $result = $lists::select('quotes.*', 'quotes.id as quote_id', 'items.*', 'items.description as item_description', 'brands.name as brand', 'items_type.name as item_type', 'items_type.*', 'sellers.*', 'status.*')
            ->join('quotes', 'quotes.id', '=', 'lists.quotes_id')
            ->join('items', 'items.id', '=', 'quotes.item_id')
            ->join('brands', 'brands.id', '=', 'items.brand_id')
            ->join('items_type', 'items_type.id', '=', 'items.item_type_id')
            ->join('sellers', 'sellers.id', '=', 'quotes.seller_id')
            ->join('status', 'status.id', '=', 'quotes.status_id')
            ->where('quotes.id', '=', $id)
            ->first();

        return $result ? $result : null;
    }

    /**
     * Get quote status
     * @return Object
     */
    public function getQuoteStatus()
    {
        return Status::all();
    }

    /**
     * Get item type
     * @return Object
     */
    public function getItemType()
    {
        return Items_type::all();
    }

    /**
     * Get brands
     * @return Object
     */
    public function getBrands()
    {
        return Brands::all();
    }

    /**
     * Update quote items
     * @param array
     * @return Object
     */
    public function updateQuoteItems($request)
    {
        $quote = Quotes::find($request->quote_id);
        $this->updateQuoteStatus($quote->id, $request);
        $this->updateSeller($quote->seller_id, $request);
        $this->updateItemType($quote->item_id, $request);
        $this->updateDesigner($quote->item_id, $request);
        $this->updateItem($quote->item_id, $request);
    }

    /**
     * Update quote status
     * @param array
     * @return Object
     */
    public function updateQuoteStatus($quoteId, $request)
    {
        $quote = new Quotes();
        $quoteUpdate = $quote::find($quoteId);
        $quoteUpdate->status_id = $request->quote_status;
        $quoteUpdate->save();
        return;
    }

    /**
     * Update seller field
     * @param array
     * @return Object
     */
    public function updateSeller($sellerId, $request)
    {
        $seller = new Sellers();
        $seller = $seller::find($sellerId);
        $seller->first_name = $request->first_name;
        $seller->last_name = $request->last_name;
        $seller->email = $request->email;
        $seller->phone1 = $request->phone;
        $seller->save();
        return;
    }

    /**
     * Update items_type
     * @param array
     * @return Object
     */
    public function updateItemType($itemId, $request) {
        $item = $this->getItemsById($itemId);
        $item->item_type_id = $request->item_category;
        $item->save();
        return;
    }

    /**
     * Update brands
     * @param array
     * @return Object
     */
    public function updateDesigner($itemId, $request) {
        $item = $this->getItemsById($itemId);
        $item->brand_id = $request->designer;
        $item->save();
        return;
    }

    /**
     * Update items table
     * @param array
     * @return Object
     */
    public function updateItem($itemId, $request) {
        $item = $this->getItemsById($itemId);
        $item->description = $request->description;
        $item->condition = $request->condition;
        $item->size = $request->size;
        $item->save();
        return;
    }

    /**
     * get items by id
     * @param int
     * @return Object
     */
    public function getItemsById($itemId) {
        $item = new Items();
        return $item::find($itemId);
    }
}
