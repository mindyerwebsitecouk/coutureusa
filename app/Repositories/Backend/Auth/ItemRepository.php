<?php

namespace App\Repositories\Backend\Auth;

use DB;
use Carbon\Carbon;
use App\Models\Auth\Products;
use App\Models\Auth\Lists;
use App\Models\Auth\Quotes;
use App\Models\Auth\Items;
use App\Models\Auth\Items_type;
use App\Models\Auth\Item_images;
use App\Models\Auth\Brands;
use App\Models\Auth\Sellers;
use App\Models\Auth\Status;

/**
 * Class ItemRepository.
 */
class ItemRepository
{
    /**
     * Insert Item image
     * @param Object
     * @return Object
     */
    public function insertItemImage($itemId, $uploadSavePath) {
        $itemImages = new Item_images();
        $itemImages->item_id = $itemId;
        $itemImages->image_name = $uploadSavePath;

        return $itemImages->save() ? true : false;
    }

    public function getItemImage($itemId) {
        $itemImage = new Item_images();
        $result = $itemImage::where('item_id', $itemId)->first();

        return $result ? $result : null;
    }

    public function getItems() {
        $items = new Items();
        $result = $items::select('brands.name as brands_name', 'items_type.name as items_type_name', 'quotes.price', 'quotes.updated_at as quotes_updated_at', 'item_status.name as item_status_name')
            ->join('brands', 'brands.id', '=', 'items.brand_id')
            ->join('items_type', 'items_type.id', '=', 'items.item_type_id')
            ->join('quotes', 'quotes.item_id', '=', 'items.id')
            ->join('item_status', 'item_status.id', '=', 'items.item_status')
            ->get();

        return $result ? $result : null;
    }
}
