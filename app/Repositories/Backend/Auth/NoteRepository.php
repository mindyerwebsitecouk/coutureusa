<?php

namespace App\Repositories\Backend\Auth;

use DB;
use Carbon\Carbon;
use App\Models\Auth\Notes;


/**
 * Class NoteRepository.
 */
class NoteRepository
{
     /**
     * save data on note table
     * @param array
     * @return Object
     */
    public function insertNote($request) {
        $noteInsert = new Notes();
        $noteInsert->quote_id = $request->quote_id;
        $noteInsert->status_id = $request->quote_status;
        $noteInsert->user_id = $request->user_id;
        $noteInsert->created_at = Carbon::now()->toDateTimeString();
        $noteInsert->updated_at = Carbon::now()->toDateTimeString();

        return $noteInsert->save() ? true : false;
    }

     /**
     * get note logs from note table
     * @param array
     * @return Object
     */
    public function getNote($quoteId) {
        $note = new Notes();
        $result = $note::select('notes.created_at', 'users.first_name', 'users.last_name', 'quotes.id as quote_id', 'status.name as status_name', 'status.description')
        ->join('users', 'users.id', '=', 'notes.user_id')
        ->join('quotes', 'quotes.id', '=', 'notes.quote_id')
        ->join('status', 'status.id', '=', 'notes.status_id')
        ->where('notes.quote_id', $quoteId)
        ->get();

        return $result ? $result : null;
    }
}
