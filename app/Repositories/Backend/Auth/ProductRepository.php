<?php

namespace App\Repositories\Backend\Auth;

use DB;
use Carbon\Carbon;
use App\Models\Auth\Products;
use App\Models\Auth\Lists;
use App\Models\Auth\Quotes;
use App\Models\Auth\Items;
use App\Models\Auth\Items_type;
use App\Models\Auth\Brands;
use App\Models\Auth\Sellers;

/**
 * Class ProductRepository.
 */
class ProductRepository
{

    protected $defaultPendingStatus = 1;

    /**
     * Get Active Item list
     *
     * @return Object
     */
    public function getItems()
    {
        $lists = new Lists();
        $result = $lists::select('lists.quotes_id as quoteId', 'items.name as item', 'brands.name as brand', 'items_type.name as item_type', 'sellers.first_name as seller_first', 'sellers.last_name as seller_last', 'status.name as status', 'quotes.created_at as submitDate', 'quotes.status_id')
            ->join('quotes', 'quotes.id', '=', 'lists.quotes_id')
            ->join('items', 'items.id', '=', 'quotes.item_id')
            ->join('brands', 'brands.id', '=', 'items.brand_id')
            ->join('items_type', 'items_type.id', '=', 'items.item_type_id')
            ->join('sellers', 'sellers.id', '=', 'quotes.seller_id')
            ->join('status', 'status.id', '=', 'quotes.status_id')
            ->get();

            return $result ? $result : null;
    }

    /**
     * Propose items from API
     */
    public function proposeItem($contentObject)
    {
        $status = false;
        try {
            $sellerId = $this->insertSellersTable($contentObject->items);
            if ($sellerId) {
                $itemId = $this->insertItemsTable($contentObject->items);
                if ($itemId) {
                    $quoteId = $this->insertQuotesTable($itemId, $sellerId, $contentObject->items);
                    if ($quoteId) {
                        $listsId = $this->insertListsTable($quoteId, $contentObject->items);
                        if ($listsId)
                            $status = true;
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }

    /**
     * Insert sellers table
     * @return int
     */
    public function insertSellersTable($contentObject)
    {
        $sellers = new Sellers();
        $sellers->first_name = $contentObject->first_name;
        $sellers->last_name = $contentObject->last_name;
        $sellers->email = $contentObject->email;

        return $sellers->save() ? $sellers->id : null;
    }

    /**
     * Insert item table
     * @return int
     */
    public function insertItemsTable($contentObject)
    {
        $items = new Items();
        $items->item_type_id = $this->getItemTypeId($contentObject->item_type);
        $items->brand_id = $this->getBrandId($contentObject->brand);
        $items->description = $contentObject->description;
        $items->condition = $contentObject->condition;
        $items->size = $contentObject->size;
        $items->created_at = Carbon::now()->toDateTimeString();
        $items->updated_at = Carbon::now()->toDateTimeString();
        return $items->save() ? $items->id : null;
    }

    /**
     * get brand_id id from table
     * @return int
     */
    public function getBrandId($brand)
    {

        $brands = new Brands();
        $brandsResult = $brands::where('name', ucfirst($brand))->first();
        if ($brandsResult) {
            return $brandsResult->id;
        } else {
            return $this->insertBrands($brand);
        }
    }
    /**
     * insert brands table
     * @return int
     */
    public function insertBrands($brand)
    {
        $brands = new Brands();
        $brands->name = $brand;
        return $brands->save() ? $brands->id : null;
    }

    /**
     * get item_type id from table
     * @return int
     */
    public function getItemTypeId($item_type)
    {

        $itemsType = new Items_type();
        $itemsTypeResult = $itemsType::where('name', ucfirst($item_type))->first();
        if ($itemsTypeResult) {
            return $itemsTypeResult->id;
        } else {
            return $this->insertItemsType($item_type);
        }
    }

    /**
     * insert items type table
     * @return int
     */
    public function insertItemsType($item_type)
    {
        $itemsType = new Items_type();
        $itemsType->name = $item_type;
        return $itemsType->save() ? $itemsType->id : null;
    }

    /**
     * Insert quotes table
     * @return int
     */
    public function insertQuotesTable($itemId, $sellerId, $contentObject)
    {
        $quotes = new Quotes();
        $quotes->item_id = $itemId;
        $quotes->seller_id = $sellerId;
        $quotes->status_id = $this->defaultPendingStatus;
        $quotes->created_at = Carbon::now()->toDateTimeString();
        $quotes->updated_at = Carbon::now()->toDateTimeString();
        return $quotes->save() ? $quotes->id : null;
    }

    /**
     * Insert lists table
     * @return int
     */
    public function insertListsTable($quoteId, $contentObject)
    {
        $lists = new Lists();
        $lists->quotes_id = $quoteId;
        $lists->created_at = Carbon::now()->toDateTimeString();
        $lists->updated_at = Carbon::now()->toDateTimeString();
        return $lists->save() ? $lists->id : null;
    }
}
